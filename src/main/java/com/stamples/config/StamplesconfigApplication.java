package com.stamples.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StamplesconfigApplication {

	public static void main(String[] args) {
		SpringApplication.run(StamplesconfigApplication.class, args);
	}
}
